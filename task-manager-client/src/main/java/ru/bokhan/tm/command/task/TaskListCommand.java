package ru.bokhan.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bokhan.tm.command.AbstractCommand;
import ru.bokhan.tm.endpoint.Session;
import ru.bokhan.tm.endpoint.Task;
import ru.bokhan.tm.exception.security.AccessDeniedException;

import java.util.List;

public final class TaskListCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "task-list";
    }

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Show task list";
    }

    @Override
    public void execute() {
        @Nullable final Session session = endpointLocator.getCurrentSession();
        if (session == null) throw new AccessDeniedException();
        System.out.println("[LIST TASKS]");
        @NotNull final List<Task> tasks = endpointLocator.getTaskEndpoint().findTaskAll(session);
        for (@Nullable final Task task : tasks) System.out.println(task);
        System.out.println("[OK]");
    }

}
