package ru.bokhan.tm.command.data.xml;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bokhan.tm.command.AbstractCommand;
import ru.bokhan.tm.endpoint.DomainEndpoint;
import ru.bokhan.tm.endpoint.Session;
import ru.bokhan.tm.exception.security.AccessDeniedException;

public final class DataXmlSaveCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "data-xml-save";
    }

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Save data to xml file.";
    }

    @Override
    public void execute() {
        @Nullable final Session session = endpointLocator.getCurrentSession();
        if (session == null) throw new AccessDeniedException();
        System.out.println("[DATA XML SAVE]");
        @NotNull final DomainEndpoint domainEndpoint = endpointLocator.getDomainEndpoint();
        if (domainEndpoint.saveToXml(session)) System.out.println("[OK]");
    }

}
