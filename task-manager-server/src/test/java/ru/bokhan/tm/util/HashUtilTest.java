package ru.bokhan.tm.util;

import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;

import static ru.bokhan.tm.constant.UserTestData.USER1;

public final class HashUtilTest {

    @Test
    public void salt() {
        @Nullable final String result1 = HashUtil.md5(USER1.getPasswordHash());
        Assert.assertNotNull(result1);
        @Nullable final String result2 = HashUtil.md5(USER1.getPasswordHash());
        Assert.assertNotNull(result2);
        Assert.assertEquals(result1, result2);
    }

    @Test
    public void md5() {
        @Nullable final String result1 = HashUtil.md5(USER1.getPasswordHash());
        Assert.assertNotNull(result1);
        @Nullable final String result2 = HashUtil.md5(USER1.getPasswordHash());
        Assert.assertNotNull(result2);
        Assert.assertEquals(result1, result2);
    }
}