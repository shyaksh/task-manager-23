package ru.bokhan.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bokhan.tm.entity.Session;
import ru.bokhan.tm.entity.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.util.List;

public interface ISessionEndpoint {

    @WebMethod
    void closeSession(
            @WebParam(name = "session") @Nullable Session session
    );

    @WebMethod
    void closeSessionAll(
            @WebParam(name = "session") @Nullable Session session
    );

    @Nullable
    @WebMethod
    User getUserBySession(
            @WebParam(name = "session") @Nullable Session session
    );

    @NotNull
    @WebMethod
    String getUserIdBySession(
            @WebParam(name = "session") @Nullable Session session
    );

    @NotNull
    @WebMethod
    List<Session> findSessionAll(
            @WebParam(name = "session") @Nullable Session session
    );

    @Nullable
    @WebMethod
    Session openSession(
            @WebParam(name = "login") @Nullable String login,
            @WebParam(name = "password") @Nullable String password
    );

    @WebMethod
    void signOutByLogin(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "login") @Nullable String login
    );

    @WebMethod
    void signOutByUserId(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "userId") @Nullable String userId
    );

    @WebMethod
    void removeSessionAll(
            @WebParam(name = "session") @Nullable Session session
    );

    @Nullable
    @WebMethod
    Session removeSession(
            @WebParam(name = "session") @Nullable Session session
    );

}
