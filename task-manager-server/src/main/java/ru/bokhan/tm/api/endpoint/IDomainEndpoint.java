package ru.bokhan.tm.api.endpoint;

import org.jetbrains.annotations.Nullable;
import ru.bokhan.tm.entity.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;

public interface IDomainEndpoint {

    @WebMethod
    boolean saveToXml(@WebParam(name = "session") @Nullable Session session);

    @WebMethod
    boolean loadFromXml(@WebParam(name = "session") @Nullable Session session);

    @WebMethod
    boolean removeXml(@WebParam(name = "session") @Nullable Session session);

    @WebMethod
    boolean saveToJson(@WebParam(name = "session") @Nullable Session session);

    @WebMethod
    boolean loadFromJson(@WebParam(name = "session") @Nullable Session session);

    @WebMethod
    boolean removeJson(@WebParam(name = "session") @Nullable Session session);

    @WebMethod
    boolean saveToBinary(@WebParam(name = "session") @Nullable Session session);

    @WebMethod
    boolean loadFromBinary(@WebParam(name = "session") @Nullable Session session);

    @WebMethod
    boolean removeBinary(@WebParam(name = "session") @Nullable Session session);

    @WebMethod
    boolean saveToBase64(@WebParam(name = "session") @Nullable Session session);

    @WebMethod
    boolean loadFromBase64(@WebParam(name = "session") @Nullable Session session);

    @WebMethod
    boolean removeBase64(@WebParam(name = "session") @Nullable Session session);

}
