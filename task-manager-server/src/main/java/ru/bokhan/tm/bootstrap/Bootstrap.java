package ru.bokhan.tm.bootstrap;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bokhan.tm.api.IServiceLocator;
import ru.bokhan.tm.api.repository.IProjectRepository;
import ru.bokhan.tm.api.repository.ISessionRepository;
import ru.bokhan.tm.api.repository.ITaskRepository;
import ru.bokhan.tm.api.repository.IUserRepository;
import ru.bokhan.tm.api.service.*;
import ru.bokhan.tm.endpoint.*;
import ru.bokhan.tm.entity.User;
import ru.bokhan.tm.enumerated.Role;
import ru.bokhan.tm.repository.ProjectRepository;
import ru.bokhan.tm.repository.SessionRepository;
import ru.bokhan.tm.repository.TaskRepository;
import ru.bokhan.tm.repository.UserRepository;
import ru.bokhan.tm.service.*;

import javax.xml.ws.Endpoint;

@Getter
public final class Bootstrap implements IServiceLocator {

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final IUserService userService = new UserService(userRepository);

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @NotNull
    private final IDomainService domainService = new DomainService(
            userService, projectService, taskService
    );

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final ISessionRepository sessionRepository = new SessionRepository();

    @NotNull
    private final ISessionService sessionService = new SessionService(
            sessionRepository, userService, propertyService
    );

    @NotNull
    private final UserEndpoint userEndpoint = new UserEndpoint(this);

    @NotNull
    private final TaskEndpoint taskEndpoint = new TaskEndpoint(this);

    @NotNull
    private final ProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

    @NotNull
    private final SessionEndpoint sessionEndpoint = new SessionEndpoint(this);

    @NotNull
    private final DomainEndpoint domainEndpoint = new DomainEndpoint(this);

    private void initData() {
        @NotNull final User user = userService.create("user", "user", "user@test.ru");
        taskService.create(user.getId(), "UserTask1");
        taskService.create(user.getId(), "UserTask2");
        projectService.create(user.getId(), "UserProject1");
        projectService.create(user.getId(), "UserProject2");
        @NotNull final User admin = userService.create("admin", "admin", Role.ADMIN);
        taskService.create(admin.getId(), "AdminTask1");
        taskService.create(admin.getId(), "AdminTask2");
        projectService.create(admin.getId(), "AdminProject1");
        projectService.create(admin.getId(), "AdminProject2");
    }

    private void initEndpoint() {
        registry(sessionEndpoint);
        registry(taskEndpoint);
        registry(projectEndpoint);
        registry(userEndpoint);
        registry(domainEndpoint);
    }

    private void initProperty() {
        propertyService.init();
    }

    private void registry(@Nullable final Object endpoint) {
        if (endpoint == null) return;
        @NotNull String host = propertyService.getServerHost();
        @NotNull Integer port = propertyService.getServerPort();
        @NotNull String name = endpoint.getClass().getSimpleName();
        @NotNull String wsdl = "http://" + host + ":" + port + "/" + name + "?wsdl";
        System.out.println(wsdl);
        Endpoint.publish(wsdl, endpoint);
    }

    public void run(@Nullable final String[] arguments) {
        System.out.println("** TASK MANAGER SERVER **");
        initProperty();
        initData();
        initEndpoint();
    }

}